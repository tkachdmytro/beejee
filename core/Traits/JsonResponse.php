<?php
namespace App\Traits;

trait JsonResponse
{
    protected $statuses = [
        422 => 'Unprocessable entity',
        500 => 'Internal error',
        404 => 'Not Found.',
        200 => 'OK',
        400 => 'Bad Request',
        403 => 'Access denied'
    ];

    /**
     * @var array
     */
    protected $response = ['success' => true, 'message' => ''];

    public function json($data, $status = 200, $message = '')
    {
        ob_end_clean();
        $message = $this->statuses[$status];

        header($_SERVER['SERVER_PROTOCOL'] . ' ' . $status . ' ' . $message);

        http_response_code($status);
        header('Content-Type: application/json;charset=utf-8');
        
        return return_json($data);
    }

    public function response($status = 200, $message = '')
    {
        return $this->json($this->getResponse());
    }

    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    public function getResponse()
    {
        return $this->response;
    }
}