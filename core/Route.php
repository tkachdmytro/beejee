<?
namespace App;

class Route
{
    static public $current_page = "main";

    static function start()
    {
        $controller_name = "Main";

        $request_uri = explode('?', $_SERVER['REQUEST_URI']);
        $routes = explode('/', $request_uri[0]);
        if (!empty($routes[1])) {
            if ($routes[1] == "404") {
                $controller_name = "Error404";
            } else {
                $controller_name = ucfirst($routes[1]);
            }
            self::$current_page = $routes[1];
        }
        
        if (!empty($routes[2]) ){
            $action = $routes[2];
        } else {
            $action = "index";
        }

        $full_controller_name = "App\Controllers\\".$controller_name;
        $controller = $full_controller_name::getInstance();

        if(method_exists($controller, $action)) {
            $controller->$action();
        }else{
            self::errorPage404();
        }
    }

    public static function errorPage404(){
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }

    public static function getCurrentPage(){
        return self::$current_page;
    }
}
