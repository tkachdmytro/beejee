<?php
class Autoloader
{
    protected $namespacesMap = array();
 
    public function addNamespace($namespace, $rootDir)
    {
        if (is_dir($rootDir)) {
            $this->namespacesMap[$namespace] = $rootDir;
            return true;
        }
        
        return false;
    }
    
    public function register()
    {
        spl_autoload_register(array($this, 'autoload'));
    }
    
    protected function autoload($class)
    {
        //var_dump($class);
        $pathParts = explode('\\', $class);
        if (is_array($pathParts)) {
            $fileName = array_pop($pathParts);
            $namespace = implode('\\', $pathParts);

            //var_dump($fileName, $namespace, 1111111);

            if (!empty($this->namespacesMap[$namespace])) {
                $filePath = $this->namespacesMap[$namespace]. '/' . $fileName . '.php'; 
                if(file_exists($filePath)){
                    //var_dump($filePath);
                    require_once $filePath;

                    return true;
                }else{
                    return false;
                }
            }
        }
        
        return false;
    }
 
}