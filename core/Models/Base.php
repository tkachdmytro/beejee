<?
namespace App\Models;

abstract class Base{
	
	public $db;
	public $query = "";
	public $statement;
	public static $instance;

	function __construct(){
		$this->db = new \PDO(
			'mysql:host=localhost;dbname='. $this->getDatabaseName(), 
			'beejee', 
			'beejee', 
			array(
				\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
			)
		);
	}

	public function getDatabaseName() {
        return 'beejee';
    }

    public function selectAll() {
        $this->query = "SELECT * FROM {$this->getTableName()} ";

        return $this;
    }

    public function select($arFields = array()) {
        $fields = implode(",", $arFields);
        $this->query = "SELECT {$fields} FROM {$this->getTableName()} ";

        return $this;
    }

    public function where($arFilter = array()) {
        $where = "1=1";
        foreach ($arFilter as $key => $value) {
            $where .= " AND ".$key."='".$value."'";
        }
        $this->query .= "WHERE {$where} ";

        return $this;
    }

    public function order($order) {
    	$this->query .= "ORDER BY ".$order;

        return $this;
    }

    public function sort($sort = "ASC") {
        $this->query .= " ".$sort;
    	
        return $this;
    }

    public function limit($limit) {
        $this->query .= " LIMIT ".$limit;
    	
        return $this;
    }

    public function offset($offset) {
        $this->query .= " OFFSET ".$offset;
    	
        return $this;
    }

    public function getQueryStr() {
        return $this->query;
    }

    public function query() {
    	$this->statement = $this->db->query($this->query); 

        return $this;
    }

    public function rowCount() {
        if ($this->statement) {
            return $this->statement->rowCount();
        }

        return 0;
    }

    public function fetch() {
        if ($this->statement) {
            return $this->statement->fetch();
        }

        return false;
    }
}