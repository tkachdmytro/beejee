<?
namespace App\Models;

class Tasks extends Base
{
    private $limit = 3;

    public function getTableName()
    {
        return 'tasks';
    }

    public function getFullTableName()
    {
        return $this->getDatabaseName() . "." . $this->getTableName();
    }

    public function getTableAllRowsCount()
    {
        return $this->selectAll()->query()->rowCount();
    }

    public function getTablePageCount()
    {
        return ceil($this->getTableAllRowsCount() / $this->limit);
    }

    public function getDataById($id)
    {
        $arFilter = array("id" => $id);

        return $this->selectAll()->where($arFilter)->query()->fetch();
    }

    public function getData($order_by = 'id', $sort_by = 'ASC', $page = 1)
    {
        $offset = ($page-1)*3;

        $statement = $this->selectAll()->order($order_by)->sort($sort_by)->limit($this->limit)->offset($offset)->query();

        $arResult = array();
        while ($row = $statement->fetch()){
            $arResult[] = $row; 
        }
        return $arResult;
    }

    public function create($data)
    {
        $status = "new";

        $statement = $this->db->prepare("INSERT INTO {$this->getTableName()} ( `name`, `email`, `description`, `status`) VALUES (?,?,?,?)");
        $statement->bindParam(1, $data['name']);
        $statement->bindParam(2, $data['email']);
        $statement->bindParam(3, $data['description']);
        $statement->bindParam(4, $status);

        return $statement->execute();
    }

    public function update($data)
    {
        $oldData = $this->getDataById($data['id']);

        if ($oldData['description'] != $data['description']) {
            $data['admin_edit'] = "yes";
        } else {
            $data['admin_edit'] = "no";
        }

        if (empty($data['status'])) {
            $data['status'] = "new";
        }
        $sql = "UPDATE  " . $this->getFullTableName() . " SET description=?, status=?, admin_edit=? WHERE id=?";
        $statement = $this->db->prepare($sql);

        return $statement->execute(array($data['description'], $data['status'], $data['admin_edit'], $data['id']));
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
