<?
namespace App\Models;

class Users extends Base
{
    public function getTableName() {
        return 'users';
    }

    public function getDataById($id) {
        $arFilter = array("id" => $id);

        return $this->selectAll()->where($arFilter)->query()->fetch();
    }

    public function authCheck($data){
        if(
            !empty($data['login']) 
            && 
            !empty($data['password'])
        ) {     
            $arFilter = array(
                "login" => $data['login'],
                "password" => $data['password'],
            );
            
            return $this->selectAll()->where($arFilter)->query()->fetch();
        }

        return false;
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
