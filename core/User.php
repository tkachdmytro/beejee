<?
namespace App;

class User
{
    public $session;
    public static $instance;

    function __construct()
    {
        $this->session = $_SESSION;
    }


    public function isAdmin()
    {
        return $this->session["IS_AUTHORIZED"];
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
