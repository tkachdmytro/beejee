<?
namespace App;

class View
{
    public static $instance;
    
    public function render($content_view, $data = array())
    {
        include $_SERVER["DOCUMENT_ROOT"].'/templates/header.php';
        include $_SERVER["DOCUMENT_ROOT"].'/templates/'.$content_view.'.php';
        include $_SERVER["DOCUMENT_ROOT"].'/templates/footer.php';
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
