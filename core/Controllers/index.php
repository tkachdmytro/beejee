<?php
if(session_status() != PHP_SESSION_ACTIVE) {
	session_start();
} 

require_once $_SERVER["DOCUMENT_ROOT"].'/core/Autoloader.php';

$autoloader = new Autoloader();
$autoloader->addNamespace('App', __DIR__ . '/core');
$autoloader->addNamespace('App\\Controllers', __DIR__ . '/core/Controllers');
$autoloader->addNamespace('App\\Models', __DIR__ . '/core/Models');
$autoloader->addNamespace('App\\Traits', __DIR__ . '/core/Traits');
$autoloader->register();

App\Route::start();