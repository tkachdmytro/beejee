<?
namespace App\Controllers;

use App\View;

class Error404 extends Base
{
    function __construct(){
        $this->view = View::getInstance();
    }

    public function index(){
        $this->view->render('404');
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
