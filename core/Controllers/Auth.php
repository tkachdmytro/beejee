<?
namespace App\Controllers;

use App\View;
use App\Models;

class Auth extends Base
{
    function __construct(){
        $this->model = Models\Users::getInstance();
        $this->view = View::getInstance();
    }

    public function index(){
        $this->view->render('auth');
    }

    public function login(){
        $result = $this->model->authCheck($this->post());
        if ($result) {
            $_SESSION["IS_AUTHORIZED"] = true;
            $json = array('succses' => true);
        } else {
            $_SESSION["IS_AUTHORIZED"] = false;
            $json = array('succses' => false);
        }
        echo json_encode($json);
    }

    public function logout(){
        $_SESSION["IS_AUTHORIZED"] = false;
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('Location:'.$host);
        exit;
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
