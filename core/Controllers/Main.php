<?php
namespace App\Controllers;

use App\User;
use App\View;
use App\Models;

class Main extends Base 
{
    function __construct()    {
        $this->model = Models\Tasks::getInstance();
        $this->view = View::getInstance();
        $this->user = User::getInstance();
    }

    function index(){
        $request = $this->request();
        switch (true) {
            case !empty($request['page']) && !empty($request['sort']) && !empty($request['order']):
                $data = $this->model->getData(
                    $request['order'], 
                    $request['sort'], 
                    $request['page']
                );
                break;
            case !empty($request['page']):
                $data = $this->model->getData(
                    'id', 
                    'asc', 
                    $request['page']
                );  
                break;

            case !empty($request['sort']) && !empty($request['order']):
                $data = $this->model->getData(
                    $request['order'], 
                    $request['sort']
                ); 
                break;
            
            default:
                $data = $this->model->getData();
                break;
        }
        $arResult["isAdmin"] = $this->user->isAdmin();
        $arResult["tasks"] = $data;
        $arResult["sortBy"] =  !empty($request['sort'])?$request['sort']:'asc';
        $arResult["orderBy"] = !empty($request['order'])?$request['order']:'id';
        $arResult["pagesCount"] = $this->model->getTablePageCount();
        if(!empty($request['page'])){
            $arResult["pageNumber"] = $request['page'];
        }else{
            $arResult["pageNumber"] = 1;
        }
        $arResult["urlParams"] = "";
        if (!empty($request['sort'])) {
            $arResult["urlParams"] = "sort=".$request['sort']."&";
        }
        if (!empty($request['order'])) {
            $arResult["urlParams"] .= "order=".$request['order']."&";
        }
        
        $this->view->render('main', $arResult);
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }
}