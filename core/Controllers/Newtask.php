<?
namespace App\Controllers;

use App\View;
use App\Models;

class Newtask extends Base
{
    function __construct(){
        $this->model = Models\Tasks::getInstance();
        $this->view = View::getInstance();
    }
    
    public function index(){
        $this->view->render('newtask');
    }

    public function save(){
        $result = $this->model->create($this->post());
        if($result){
            $json = array('succses' => true);
        }else{
            $json = array('succses' => false);
        }
        echo json_encode($json);    
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
