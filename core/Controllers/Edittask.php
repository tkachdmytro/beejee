<?
namespace App\Controllers;

use App\View;
use App\Models;

class Edittask extends Base
{
    function __construct(){
        $this->model = Models\Tasks::getInstance();
        $this->view = View::getInstance();
    }
    
    public function index(){
        if(!empty($_REQUEST['id'])){
            $data = $this->model->getDataById($_REQUEST['id']);
            $this->view->render('edittask', $data);
        }
    }

    public function update(){
        $result = $this->model->update($this->post());
        if($result){
            $json = array('succses' => true);
        }else{
            $json = array('succses' => false);
        }
        echo json_encode($json);    
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
