<?php
namespace App\Controllers;

use App\Traits\Request;
use App\Traits\JsonResponse;


abstract class Base
{
	use Request;
	use JsonResponse;

    public $model;
    public $view;
    public static $instance;

    abstract public function index();
    
    abstract public static function getInstance();
}
