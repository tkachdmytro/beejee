﻿window.onload = function() {
    var input = document.getElementById('inputGroupFile01');
    if(input){
        input.addEventListener('change', function(){
            document.getElementById('inputGroupFile01Lable').innerHTML=$('#inputGroupFile01').val();
        }, false);    
    }

    var setTimeoutAuth = false;
    $('body').on('submit', '#authForm', function(e){
        e.preventDefault();
        $.post($(this).attr("action"), $(this).serialize(), function(response) {
            response = JSON.parse(response);
            if(response.succses){
                $("#result").html('<div class="alert alert-success">You are logged in!</div>');
                if(typeof setTimeoutAuth != 'undefined') {
                    window.clearTimeout(setTimeoutAuth);
                }
                setTimeoutAuth = window.setTimeout(function(){
                    location.href = "/";
                },1000);
            }else{
                $("#result").html('<div class="alert alert-danger" role="alert">Please, enter correct login or password!</div>');
            }
        });
    });

    var setTimeoutSave = false;
    $('body').on('submit', '#saveForm', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            data: new FormData(this),
            url: $(this).attr("action"),
            processData: false,
            contentType: false,
            success: function (response) {
                response = JSON.parse(response);
                if(response.succses){
                    $("#result").html('<div class="alert alert-success">Task save!</div>');
                    if(typeof setTimeoutSave != 'undefined') {
                        window.clearTimeout(setTimeoutUpdate);
                    }
                    setTimeoutSave = window.setTimeout(function(){
                        location.href = "/";
                    },1000);
                }else{
                    $("#result").html('<div class="alert alert-danger" role="alert">Task not save!</div>');
                }
            }
        });
    });

    var setTimeoutUpdate = false;
    $('body').on('submit', '#updateForm', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            data: new FormData(this),
            url: $(this).attr("action"),
            processData: false,
            contentType: false,
            success: function (response) {
                response = JSON.parse(response);
                if(response.succses){
                    $("#result").html('<div class="alert alert-success">Task save!</div>');
                    if(typeof setTimeoutAuth != 'undefined') {
                        window.clearTimeout(setTimeoutUpdate);
                    }
                    setTimeoutUpdate = window.setTimeout(function(){
                        location.href = "/";
                    },1000);
                }else{
                    $("#result").html('<div class="alert alert-danger" role="alert">Task not save!</div>');
                }
            }
        });
    });
};