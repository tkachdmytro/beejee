<?
use App\Route;
use App\User;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <title>Beejee task</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <link rel="stylesheet" type="text/css" href="/css/style.css?v=4" />
        <script type="text/javascript" src="/js/jquery.cookie.js"></script>        
        <script type="text/javascript" src="/js/script.js?v=1"></script>
    </head>
    <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar1">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?=(Route::getCurrentPage() == 'main') ? 'active' : '' ?>">
                    <a class="nav-link" href="/">Main</a>
                </li>
                <li class="nav-item <?=(Route::getCurrentPage() == 'newtask') ? 'active' : '' ?>">
                    <a class="nav-link" href="/newtask">New Task</a>
                </li>
                <? if (User::getInstance()->isAdmin()) { ?>
                    <li class="nav-item <?=(Route::getCurrentPage() == 'auth') ? 'active' : '' ?>">
                        <a class="nav-link" href="/auth/logout">Logout</a>
                    </li>
                <? } else { ?>
                    <li class="nav-item <?=(Route::getCurrentPage() == 'auth') ? 'active' : '' ?>">
                        <a class="nav-link" href="/auth">Login</a>
                    </li>
                <? } ?>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="row justify-content-center">