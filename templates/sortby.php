<?
$sortSelecct = array(
    "Choose..." => array(
        "url" => "/",
        'selected' =>  $data["sortBy"] == "asc" && $data["orderBy"] == "id"
    ),
    "Name asc" => array(
        "url" => "/?sort=asc&order=name&page=".$data["pageNumber"],
        'selected' =>  $data["sortBy"] == "asc" && $data["orderBy"] == "name"
    ),
    "Name desc" => array(
        "url" => "/?sort=desc&order=name&page=".$data["pageNumber"],
        'selected' =>  $data["sortBy"] == "desc" && $data["orderBy"] == "name"
    ),
    "Email asc" => array(
        "url" => "/?sort=asc&order=email&page=".$data["pageNumber"],
        'selected' =>  $data["sortBy"] == "asc" && $data["orderBy"] == "email"
    ),
    "Email desc" => array(
        "url" => "/?sort=desc&order=email&page=".$data["pageNumber"],
        'selected' =>  $data["sortBy"] == "desc" && $data["orderBy"] == "email"
    ),
    "Status asc" => array(
        "url" => "/?sort=asc&order=status&page=".$data["pageNumber"],
        'selected' =>  $data["sortBy"] == "asc" && $data["orderBy"] == "status"
    ),
    "Status desc" => array(
        "url" => "/?sort=desc&order=status&page=".$data["pageNumber"],
        'selected' =>  $data["sortBy"] == "desc" && $data["orderBy"] == "status"
    ),
);

?>
<div class="input-group mb-3 mt-3">
    <div class="input-group-prepend">
        <label class="input-group-text" for="input-sort">Sort by:</label>
    </div>
    <select id="input-sort" class="custom-select" onchange="location = this.value;">
        <? foreach ($sortSelecct as $key => $value) { ?>
            <option value="<?=$value["url"]?>" <?=$value['selected']?'selected':''?>><?=$key?></option>
        <? } ?>
    </select>
</div>