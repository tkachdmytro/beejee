<div class="col-md-6">
    <form id="saveForm" class="form-horizontal" action="/newtask/save/" method="post" enctype='multipart/form-data'>
        <span class="heading">NEW TASK</span>
        <div class="form-group">
            <input type="text" class="form-control" id="inputLogin" name="name" placeholder="Name" required>
        </div>
        <div class="form-group help">
            <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email" required>
        </div>
        <div class="form-group">
            <textarea name="description" id="description" class="form-control" rows="9" cols="25" required="required" placeholder="Text"></textarea>
        </div>
        <div id="result" class="form-group"></div>
        <div class="form-group row">
            <button type="submit" class="btn btn-default">SAVE</button>
        </div>
    </form>
</div>