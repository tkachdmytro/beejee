<div class="col-xl-12">
    <? include "sortby.php"?>
    <h4>Task List</h4>    
    <table class="table table-hover">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">NAME</th>
                <th scope="col">EMAIL</th>
                <th scope="col">DESCRIPTION</th>
                <th scope="col">STATUS</th>
                <?if ($data['isAdmin'] == true) {?>
                    <th scope="col">EDIT</th>
                    <th scope="col">ADMIN EDIT</th>
                <?}?>
            </tr>
        </thead>
        <tbody>
            <?foreach ($data["tasks"] as $key=>$row) {
                if ($row['status'] == "done") { 
                    $str = '<tr class="table-success">';
                } else {
                    $str = '<tr>';
                }

                $str .= '<th scope="row">'.$row['id'].'</th>';
                $str .= '<td>'.$row['name'].'</td>';
                $str .= '<td>'.$row['email'].'</td>';
                $str .= '<td>'.$row['description'].'</td>';
                $str .= '<td>'.$row['status'].'</td>';
                if ($data['isAdmin'] == true) {
                    $str .='<td><a href="/edittask/?id='.$row['id'].'">edit</a></td>';
                    $str .='<td>'.$row['admin_edit'].'</td>';
                }
                $str .= '</tr>';
                
                echo $str;
            }?>
        </tbody>
    </table>
</div>
<div class="col-xl-12">
    <nav aria-label="Page navigation example">
      <ul class="pagination">
          <?if($data["pageNumber"]-1 > 0){?>
            <li class="page-item"><a class="page-link" href="/?<?=$data["urlParams"]?>page=<?=($data["pageNumber"]-1)?>">Previous</a></li>
          <?}?>
        <?for ($i=1; $i <= $data["pagesCount"]; $i++){?>
            <li class="page-item <?=($data["pageNumber"]==$i?'active':'')?>"><a class="page-link" href="/?<?=$data["urlParams"]?>page=<?=$i?>"><?=$i?></a></li>
        <?}?>
        <?if($data["pageNumber"]+1 <= $data["pagesCount"]){?>
             <li class="page-item"><a class="page-link" href="/?<?=$data["urlParams"]?>page=<?=($data["pageNumber"]+1)?>">Next</a></li>
          <?}?>
      </ul>
    </nav>
</div>