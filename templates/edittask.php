<div class="col-md-6">
    <form id="updateForm" class="form-horizontal" action="/edittask/update/" method="post" enctype='multipart/form-data'>
        <span class="heading">EDIT TASK</span>
        <div class="form-group">
            <input type="text" class="form-control" id="inputLogin" name="name" placeholder="Name" value="<?=$data['name']?>"  disabled readonly>
        </div>
        <div class="form-group help">
            <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email" value="<?=$data['email']?>"  disabled readonly>
        </div>
        <div class="form-group">
            <textarea name="description" id="description" class="form-control" rows="9" cols="25" required="required" placeholder="Text"><?=$data['description']?></textarea>
        </div>
        <?if($data['status'] != "done"){?>
            <div class="form-group help">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="status" value="done" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        Status done
                    </label>
                </div>
            </div>
        <?}else{?>
            <div class="form-group">
                <input type="hidden" class="form-control" name="status" value="done">
            </div>
        <?}?>
        <div class="form-group">
            <input type="hidden" class="form-control" name="id" value="<?=$data['id']?>">
        </div>
        <div id="result" class="form-group"></div>
        <div class="form-group row">
            <button type="submit" class="btn btn-default">SAVE</button>
        </div>
    </form>
</div>
