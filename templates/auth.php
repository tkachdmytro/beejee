<div class="col-md-6">
    <form class="form-horizontal" action="/auth/login/" method="post" id="authForm">
        <span class="heading">Authorization</span>
        <div class="form-group">
            <input type="text" class="form-control" id="inputLogin" name="login" placeholder="Login" required>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>
        </div>
        <div id="result" class="form-group"></div>
        <div class="form-group row">
            <button type="submit" class="btn btn-default">SIGN IN</button>
        </div>
    </form>
</div>
